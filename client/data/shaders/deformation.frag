precision mediump float;

/* Rendu du jeu */
uniform sampler2D uSampler;

/* Texture de déformation en rouge et vert */
uniform sampler2D uDeformation;

/* Texture pour contrôler l'intensité de la déformation */
uniform sampler2D uIntensity;

/* Interval de temps multiplié par la vitesse depuis l'activation du composant */
uniform float uTime;

/* Échelle de la déformation */
uniform float uScale;

/* Coordonnées UV du fragment */
varying vec2 vTextureCoord;

void main(void) {
    vec2 position = vec2(uTime, 0.5);
    vec4 intensity = uScale * texture2D(uIntensity, position);
    vec2 textCoord = vTextureCoord + sin(uTime);
    vec4 deformation4 = (texture2D(uDeformation, textCoord)- 0.5) * intensity;
    vec2 deformation = vec2(deformation4.x, deformation4.y);
    gl_FragColor = texture2D(uSampler, vTextureCoord + deformation);
}
